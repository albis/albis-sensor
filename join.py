from network import LoRa
import binascii
import struct
import socket

lora = LoRa(mode=LoRa.LORAWAN)

dev_addr = struct.unpack(">l", binascii.unhexlify('00000002'))[0]
nwk_swkey = binascii.unhexlify('00000000000000000000000000000002')
app_swkey = binascii.unhexlify('00000000000000000000000000000002')

lora.join(activation=LoRa.ABP, auth=(dev_addr, nwk_swkey, app_swkey))

s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)
s.setblocking(False)
s.send(bytes([1, 9, 7, 8]))
