import pycom
import ujson
import time
import utime

conf = {}

with open("config.json", 'r') as cf:
    conf = ujson.loads(cf.read())

if conf['lora'] is True:
    from lib.loranode import LoraNodeRaw
    lora = LoraNodeRaw(conf)

if conf['wifi'] is True:
    import lib.urequests as requests

if conf['sensors']:
    from lib.sensors import Sensors
    if conf['deepsleep_shield']:
        from lib.deepsleep import DeepSleep
        ds = DeepSleep()
    else:
        from machine import deepsleep


# Start monitoring
print("Starting main loop..")
cnt = 0
while(True):
    msg = ""

    if conf['sensors']:

        sensors = Sensors(conf)
        values = []
        obs = sensors.get_obs(conf['read_cnt'])

        for ob in conf['observations']:
            if ob[1] in obs:
                if conf["debug"]:
                    print('{} = {}'.format(
                        ob[0], obs[ob[1]]
                    ))
                values.append(
                    "{}".format(obs[ob[1]])
                )

        msg = ",".join(values)

    else:
        try:
            msg = "%s,111.11,222.22,333.33,444.44,555.55,666.66" % cnt
        except Exception as e:
            print(str(e))

    if conf['lora'] is True:

        if conf['wifi'] is True:
            now = utime.gmtime(utime.time())
            # FORMAT LATEST READ
            data = "{};{}-{:02d}-{:02d}T{:02d}:{:02d}:{:02d}+0000".format(
                conf['sensorid'],
                now[0],
                now[1],
                now[2],
                now[3],
                now[4],
                now[5]
            )
            data = '{},{}'.format(
                data,
                msg
            )

        else:
            # Preparing message for lora excluding timestamp
            data = '{};{}'.format(
                conf['sensorid'],
                msg
            )

        if conf['debug']:
            print("Sending via Lora")
            print(data)

        try:
            pycom.rgbled(0xff0000)
            lora.send_msg(data)
        except Exception as e:
            print(str(e))

    if conf['wifi'] is True:
        print("Sending via WIFI")

        now = utime.gmtime(utime.time())
        # FORMAT LATEST READ
        data = "{};{}-{:02d}-{:02d}T{:02d}:{:02d}:{:02d}+0000".format(
            conf['sensorid'],
            now[0],
            now[1],
            now[2],
            now[3],
            now[4],
            now[5]
        )

        data = '{},{}'.format(
            data,
            ",".join(values)
        )

        # SEND DATA
        r = requests.post(
            conf['server'],
            data=data
        )

        if conf["debug"]:
            print(r.text)
            if r.text.find("true") > -1:
                print("data succesfully sent")
            else:
                print("error sending data")

    pycom.rgbled(0x000000)
    if conf['debug']:
        cnt += 1
        print("Sleep: %s" % cnt)
        time.sleep(conf["deepsleep_seconds"])
    else:
        print("Going to sleep")
        if conf['deepsleep_shield']:
            ds.go_to_sleep(conf["deepsleep_seconds"])
        else:
            deepsleep(conf["deepsleep_seconds"]*1000)

        print("Deepsleep ended")
