import socket
import struct
from network import LoRa

msg = 'hello lora'
_LORA_PKG_FORMAT = "!BB%ds"
_DEVICE_ID = 0x05
lora = LoRa(mode=LoRa.LORA, region=LoRa.EU868, tx_iq=True)
lora_sock = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
lora_sock.setblocking(False)
pkg = struct.pack(
    _LORA_PKG_FORMAT % len(msg),
    _DEVICE_ID,
    len(msg),
    msg
)
lora_sock.send(pkg)
