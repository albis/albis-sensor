import socket
import struct
import binascii
import time
from network import LoRa
import crypto
from crypto import AES

# A basic package header
# B: 1 byte for the deviceId
# B: 1 byte for the pkg size
# B: 1 byte for the messageId
# %ds: Formated string for string
_LORA_PKG_FORMAT = "!BB%ds"

# A basic ack package
# B: 1 byte for the deviceId
# B: 1 byte for the pkg size
# B: 1 byte for the messageId
# B: 1 byte for the Ok (200) or error messages
_LORA_PKG_ACK_FORMAT = "BBB"


class LoraNodeRaw():

    _MAX_ACK_TIME = 5000
    _RETRY_COUNT = 3

    def __init__(self, conf=None):
        # conf: {
        #    "deviceId": integer[0...255]
        # }
        self.conf = conf
        self._DEVICE_ID = self.conf['config']['lora']['deviceId']  # 0x05
        self.lora = LoRa(mode=LoRa.LORA, region=LoRa.EU868, tx_iq=True)
        self.lora_sock = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
        self.lora_sock.setblocking(False)

    # Method for acknoledge waiting time keep
    def check_ack_time(self, from_time):
        current_time = time.ticks_ms()
        return (current_time - from_time > self._MAX_ACK_TIME)

    # Method to send messages
    def send_msg(self, msg):
        retry = self._RETRY_COUNT
        while (retry > 0 and not retry == -1):
            retry -= 1
            if 'key' in self.conf['config']['lora']:
                iv = crypto.getrandbits(128)  # hardware generated random IV
                cipher = AES(
                    self.conf['config']['lora']['key'],
                    AES.MODE_CFB,
                    iv
                )
                msg = iv + cipher.encrypt(b'%s' % msg)

            pkg = struct.pack(
                _LORA_PKG_FORMAT % len(msg),
                self._DEVICE_ID,
                len(msg),
                msg
            )
            self.lora_sock.send(pkg)

            # Wait for the response from the server.
            start_time = time.ticks_ms()

            while(not self.check_ack_time(start_time)):
                recv_ack = self.lora_sock.recv(256)
                # If a message of the size of the acknoledge
                # message is received
                if (len(recv_ack) > 0):
                    device_id, pkg_len, ack = struct.unpack(
                        _LORA_PKG_ACK_FORMAT, recv_ack)
                    print("Received:")
                    print(" > device_id: %s" % device_id)
                    print(" > pkg_len: %s" % pkg_len)
                    print(" > ack: %s" % ack)
                    if (device_id == self._DEVICE_ID):
                        if (ack == 200):
                            print("ACK")
                            retry = 0
                            return

        raise Exception("Message not sent after {} attempt".format(
            self._RETRY_COUNT
        ))


class LoraNodeABP():

    _MAX_ACK_TIME = 5000
    _RETRY_COUNT = 3

    def __init__(self, conf=None):
        # conf: {
        #    "deviceId": integer[0...255]
        # }
        self._DEVICE_ID = 0x01
        self.conf = conf

        self.lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868)

        # create an ABP authentication params
        self.dev_addr = struct.unpack(">l", binascii.unhexlify('3601147D'))[0]
        self.nwk_swkey = binascii.unhexlify('3C74F4F40CAEA021303BC24284FCF3AF')
        self.app_swkey = binascii.unhexlify('0FFA7072CC6FF69A102A0F39BEB0880F')

        # remove all the non-default channels
        for i in range(3, 16):
            self.lora.remove_channel(i)

        # set the 3 default channels to the same frequency
        self.lora.add_channel(
            0, frequency=self.conf['LORA_FREQUENCY'], dr_min=0, dr_max=5)
        self.lora.add_channel(
            1, frequency=self.conf['LORA_FREQUENCY'], dr_min=0, dr_max=5)
        self.lora.add_channel(
            2, frequency=self.conf['LORA_FREQUENCY'], dr_min=0, dr_max=5)

        # join a network using ABP (Activation By Personalization)
        self.lora.join(
            activation=LoRa.ABP,
            auth=(self.dev_addr, self.nwk_swkey, self.app_swkey)
        )

        while not self.lora.has_joined():
            print('Not joined yet...')
            time.sleep(2.5)

        print("Joined!")
        # create a LoRa socket
        self.lora_sock = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
        # set the LoRaWAN data rate
        self.lora_sock.setsockopt(
            socket.SOL_LORA, socket.SO_DR, self.conf['LORA_NODE_DR'])

        # make the socket blocking
        self.lora_sock.setblocking(False)

    def increase_msg_id(self):
        self.msg_id = (self.msg_id + 1) & 0xFF

    # Method for acknoledge waiting time keep
    def check_ack_time(self, from_time):
        current_time = time.ticks_ms()
        return (current_time - from_time > self._MAX_ACK_TIME)

    # Method to send messages
    def send_msg(self, msg):

        retry = self._RETRY_COUNT
        sent = False
        while (retry > 0):
            retry -= 1
            pkg = struct.pack(
                _LORA_PKG_FORMAT % len(msg),
                self._DEVICE_ID,
                len(msg),
                msg
            )
            self.lora_sock.send(pkg)

            # Wait for the response from the server.
            start_time = time.ticks_ms()

            while(not self.check_ack_time(start_time)):
                recv_ack = self.lora_sock.recv(256)
                # If a message of the size of the acknoledge
                # message is received
                if (len(recv_ack) > 0):
                    print(recv_ack)
                    device_id, pkg_len, ack = struct.unpack(
                        _LORA_PKG_ACK_FORMAT, recv_ack)
                    if (device_id == self._DEVICE_ID+1):
                        if (ack == 200):
                            sent = True
                            # If the uart = machine.UART(0, 115200)
                            # and os.dupterm(uart) are set in the boot.py
                            # this print should appear in the serial port
                            print("ACK")
                            retry = 0
                            return

        raise Exception("Message not sent after {} attempt".format(
            self._RETRY_COUNT
        ))
